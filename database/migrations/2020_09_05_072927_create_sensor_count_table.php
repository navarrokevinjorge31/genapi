<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSensorCountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensor_count', function (Blueprint $table) {
            $table->string('count_id' , 30)->primary();
            $table->string('sensor_id')->index();
            $table->string('link_id')->index();
            $table->string('zone_id')->index();
            $table->string('node_id')->index();
            $table->string('date');
            $table->string('time');
            $table->string('count_total');
            $table->string('in_total');
            $table->string('out_total');
            $table->string('in_car');
            $table->string('in_bus');
            $table->string('in_med_truck');
            $table->string('in_large_truck');
            $table->string('in_jeepney');
            $table->string('in_bike');
            $table->string('in_tryke');
            $table->string('in_others');
            $table->string('out_car');
            $table->string('out_bus');
            $table->string('out_med_truck');
            $table->string('out_large_truck');
            $table->string('out_jeepney');
            $table->string('out_bike');
            $table->string('out_tryke');
            $table->string('out_others');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sensor_count');
    }
}
