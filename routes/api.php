<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/nodes','GeneratorController@getNodes');
Route::get('/data/{table}','GeneratorController@getTable');
Route::post('/post/{table}','GeneratorController@insertTable');
Route::get('/tables','GeneratorController@showtables');

Route::get('/greeting', function () {
    return 'Hello World';
});
