<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fct_sensorcount_peryear extends Model
{
    protected $table = 'fct_sensorcount_perweek';
    public $timestamps = false; 

    protected $fillable = [
        'count_total',
        'date',
        'id',
        'in_bike',
        'in_bus',
        'in_car',
        'in_jeepney',
        'in_large_truck',
        'in_med_truck',
        'in_others',
        'in_total',
        'in_tryke',
        'out_bike',
        'out_bus',
        'out_car',
        'out_jeepney',
        'out_large_truck',
        'out_med_truck',
        'out_others',
        'out_total',
        'out_tryke',
        'sensor_id',
        'time1',
        'time2',
    ];
}
