<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nodes;
use App\Sensors;
use Validator;
use DB;

class GeneratorController extends Controller
{
    
    public function getNodes(Request $request){
        $data['nodes'] = Nodes::all();
          return $data;
    }

    public function insertTable(Request $request , $table){

      $model_name = 'App\\'.$table;
      $model = new $model_name;

      $data = $request->all();
      $createData = $model::create($data);
      return response()->json(['message' => "Successfully Saved data to"." ".$table." table", 
                               'error' => false,
                               'error_code' => 200,
                               'line'    => "line ".__LINE__." ".basename(__FILE__)], 200); 

  //     $validator = Validator::make($request->all(),[
  //    'sensor_id' 	=> 'required',
  //    'node_id' => 'required',
  //    'sensor_location' => 'required',
  //    'survey_point' => 'required',
  //    'sensor_type' => 'required',
  //    'longitude_x' => 'required',
  //    'latitude_y' => 'required',

  //  ]);
  //  if ($validator->fails()) {
  //          return response()->json(['message' => implode(", ",$validator->messages()->all()), 
  //                                   'error' => true,
  //                                   'error_code' => 400,
  //                                   'line'    => "line ".__LINE__." ".basename(__FILE__)], 200);
  //      }
  //      $data = $request->all();
  //      $createData = $model::create($data);
  //      return response()->json(['message' => "Successfully Send data", 
  //                               'error' => false,
  //                               'error_code' => 200,
  //                               'line'    => "line ".__LINE__." ".basename(__FILE__)], 200); 
   }

    public function getTable(Request $request , $table){

      // $data['$table'] = 'App\\'.$this->table::all();
      $model_name = 'App\\'.$table;
      $model = new $model_name;

      $data[$table] = $model::all();
        // if ($table == 'sensor')
        //   $data['sensors'] = $model::all();
        // else if($table == 'nodes')
        //   $data['nodes'] = $model::all();
        
        return $data;
    }

    public function tables(Request $request){

      $table_schema = DB::select("SHOW TABLES");

      return response()->json(['message' => "Successful retrieving data", 
                                'data' => $table_schema,
                                'error' => true,
                                'error_code' => 200,
                                'line'    => "line ".__LINE__." ".basename(__FILE__)], 200);
    }

    public function showtables(){

      $database = Config('database.connections.mysql.database');
      $table_schema = DB::select("SELECT TABLE_NAME,COLUMN_NAME,COLUMN_COMMENT FROM information_schema.COLUMNS WHERE table_schema = '".$database."'");
      $table_name = [];
      foreach ($table_schema as $tables) {
          $table_names = $tables->TABLE_NAME;
          $column = $tables->COLUMN_NAME;
          $comments = $tables->COLUMN_COMMENT;
          if ($comments == "") {
             $comments = "null";
          }
          foreach ($tables as $columns) {
              $table_name[$table_names][$column] = $comments; 
         }
      }
  
      return response()->json(['message' => "Successful retrieving data", 
                              'data' => $table_name,
                              'error' => true,
                              'error_code' => 200,
                              'line'    => "line ".__LINE__." ".basename(__FILE__)], 200);

    }

    





    

}
