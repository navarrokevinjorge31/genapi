<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vehicle_locations extends Model
{
    protected $table = 'vehicle_locations';
    public $timestamps = false; 

    protected $fillable = [
        'id',
        'device_id',
        'user_id',
        'vehicle_id',
        'latitude',
        'longitude',
        'vehicle_details',
        'is_boarding',
        'is_alighting',
        'number_of_passengers',
        'date_time',
    ];
}

