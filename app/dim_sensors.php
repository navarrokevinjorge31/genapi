<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dim_sensors extends Model
{
    protected $table = 'dim_sensors';
    public $timestamps = false; 

    protected $fillable = [
        'sensor_id',
        'node_id',
        'sensor_location',
        'survey_point',
        'sensor_type',
        'longitude_x',
        'latitude_y',
    ];
}
