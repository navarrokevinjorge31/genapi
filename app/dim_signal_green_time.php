<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dim_signal_green_time extends Model
{
    protected $table = 'dim_signal_green_time';
    public $timestamps = false; 

    protected $fillable = [
        'green_time',
        'node_signal_phase',
        'triggering_condition',
    ];
}
