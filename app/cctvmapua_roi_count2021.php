<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cctvmapua_roi_count2021 extends Model
{
    protected $table = 'cctvmapua_roi_count2021';
    public $timestamps = false; 

    protected $fillable = [
        
        'count_total',
        'date_saved',
        'in_bike',
        'in_bus',
        'in_car',
        'in_jeepney',
        'in_large_truck',
        'in_med_truck',
        'in_others',
        'in_total',
        'in_tryke',
        'out_bike',
        'out_bus',
        'out_car',
        'out_jeepney',
        'out_large_truck',
        'out_med_truck',
        'out_others',
        'out_total',
        'out_tryke',
        'sensor_id',
        'time_saved',
    ];
}
