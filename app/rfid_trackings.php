<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rfid_trackings extends Model
{
    protected $table = 'rfid_trackings';
    public $timestamps = false; 

    protected $fillable = [
        'id',
        'tag_id',
        'station_id',
        'date_time',
    ];
}
