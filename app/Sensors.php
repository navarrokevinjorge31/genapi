<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensors extends Model
{
    public $timestamps = false; 
    protected $table = 'dim_sensors';

    protected $fillable = [
        'sensor_id',
        'node_id',
        'sensor_location',
        'survey_point',
        'sensor_type',
        'longitude_x',
        'latitude_y',
    ];
}
