<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dim_signal_stages extends Model
{
    protected $table = 'dim_signal_stages';
    public $timestamps = false; 

    protected $fillable = [
        'active_phase',
        'node_signal_stage',
        'offset',
        'remarks',
    ];
}
