<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dim_nodes extends Model
{
    protected $table = 'dim_nodes';
    public $timestamps = false; 

    protected $fillable = [
        'latitude_y',
        'longitude_x',
        'node_id',
        'node_name',
        'num_leg',
        'zone_id',
    ];
}
