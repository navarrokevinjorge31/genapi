<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dim_signal_phase extends Model
{
    protected $table = 'dim_signal_phase';
    public $timestamps = false; 

    protected $fillable = [
        'movement',
        'node_id',
        'node_signal_phase',
    ];
}
