<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dim_links extends Model
{
    protected $table = 'dim_links';
    public $timestamps = false; 
    
    protected $fillable = [
        'A_node',
        'B_node',
        'barangay_id',
        'cwaywidth',
        'distance',
        'elevation',
        'func_class',
        'lane_cap',
        'lane_width',
        'latitude_y',
        'link_class',
        'link_condition',
        'link_id',
        'link_name',
        'link_surface',
        'link_terrain',
        'longitude_x',
        'max_speed',
        'n_laneset',
        'num_lanes',
        'oneway',
        'right_way',
        'total_capacity',
        'zone_id',
    ];
}
