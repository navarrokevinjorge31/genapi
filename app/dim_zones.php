<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dim_zones extends Model
{
    protected $table = 'dim_zones';
    public $timestamps = false; 

    protected $fillable = [
        'cluster',
        'land_area',
        'zone_id',
        'zone_name',
    ];
}
